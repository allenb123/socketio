// Package socketio provides
// a very-incomplete client implementation
// of the socket.io protocol using
// websockets.
package socketio

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"net/http"
	"sync"
	"time"
)

// Type Client reperesents a
// connection to a socket.io
// server.
type Client struct {
	conn   *websocket.Conn
	rmutex sync.Mutex
	wmutex sync.Mutex

	eventHandlers map[string]func(data ...interface{})
}

// Function New creates a new
// Client object.
func New(url string, secure bool) (*Client, error) {
	secureChar := ""
	if secure {
		secureChar = "s"
	}

	resp, err := http.Get("http" + secureChar + "://" + url + "/socket.io/?EIO=3&transport=polling&t=" + fmt.Sprint(time.Now().Unix()*1000))
	if err != nil {
		return nil, err
	}
	cookies := resp.Cookies()
	sid := ""
	for _, cookie := range cookies {
		if cookie.Name == "io" {
			sid = cookie.Value
		}
	}

	dialer := websocket.Dialer{ReadBufferSize: 1024, WriteBufferSize: 1024}
	conn, _, err := dialer.Dial("ws"+secureChar+"://"+url+"/socket.io/?EIO=3&transport=websocket&sid="+sid, http.Header{})
	if err != nil {
		return nil, err
	}
	cl := &Client{
		conn:          conn,
		eventHandlers: make(map[string]func(data ...interface{})),
	}

	err = cl.conn.WriteMessage(websocket.TextMessage, []byte("2probe"))
	if err != nil {
		return nil, err
	}

	_, data, err := cl.conn.ReadMessage()
	if err != nil {
		return nil, err
	}

	if string(data) != "3probe" {
		return nil, fmt.Errorf("error connecting: 3probe not recieved")
	}

	err = cl.conn.WriteMessage(websocket.TextMessage, []byte("5"))
	if err != nil {
		return nil, err
	}

	_, data, err = cl.conn.ReadMessage()
	if err != nil {
		return nil, err
	}
	if string(data) != "40" {
		return nil, fmt.Errorf("error connecting: 40 not recieved")
	}

	return cl, nil
}

// Method Emit sends a message.
func (cl *Client) Emit(event string, data ...interface{}) error {
	arr := []interface{}{event}
	arr = append(arr, data...)

	body, err := json.Marshal(arr)
	if err != nil {
		return err
	}

	message := "42" + string(body)

	cl.wmutex.Lock()
	defer cl.wmutex.Unlock()

	return cl.conn.WriteMessage(websocket.TextMessage, []byte(message))
}

// Method Read reads the next message. Most
// code should not have to use this; it is
// automatically handled by Listen.
func (cl *Client) Read() (Message, error) {
	cl.rmutex.Lock()
	mt, data, err := cl.conn.ReadMessage()
	cl.rmutex.Unlock()
	if err != nil || mt == websocket.CloseMessage {
		return nil, err
	}

	if bytes.HasPrefix(data, []byte("3")) {
		return PongMessage{}, nil
	}
	if bytes.HasPrefix(data, []byte("40")) {
		return ConnectMessage{}, nil
	}
	if bytes.HasPrefix(data, []byte("41")) {
		return DisconnectMessage{}, nil
	}
	if bytes.HasPrefix(data, []byte("44")) {
		return ErrorMessage{}, nil
	}
	if bytes.HasPrefix(data, []byte("42")) {
		var out []interface{}
		err := json.Unmarshal(data[2:], &out)
		if err != nil {
			return nil, err
		}

		if len(out) == 0 {
			return nil, nil
		}

		return EventMessage{
			Event: fmt.Sprint(out[0]),
			Data:  out[1:],
		}, nil
	}

	return nil, nil
}

// Method Listen runs the event loop.
func (cl *Client) Listen() error {
	go func() {
		for {
			cl.Ping()
			time.Sleep(25 * time.Second)
		}
	}()

outer:
	for {
		msg, err := cl.Read()
		if err != nil {
			return err
		}
		switch m := msg.(type) {
		case DisconnectMessage:
			break outer
		case EventMessage:
			if cl.eventHandlers[m.Event] != nil {
				cl.eventHandlers[m.Event](m.Data...)
			}
		case PongMessage, ErrorMessage:
			continue outer
		}
	}
	return nil
}

// Method On registers an event handler. Each
// event can only have one handler.
func (cl *Client) On(event string, handler func(data ...interface{})) {
	cl.eventHandlers[event] = handler
}

// Method Ping pings the server. Most code
// shouldn't need to run this; it will
// automatically be handled by Listen.
//
// If you are writing your own event loop,
// Ping should be run
// about once every 25 seconds.
func (cl *Client) Ping() error {
	cl.wmutex.Lock()
	err := cl.conn.WriteMessage(websocket.TextMessage, []byte("2"))
	cl.wmutex.Unlock()
	if err != nil {
		return err
	}

	return nil
}

// Method Disconnect closes
// the connection.
func (cl *Client) Disconnect() {
	cl.conn.Close()
}
