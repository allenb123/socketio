package socketio

// Type Message represents a
// socketio message. It is
// returned by Read.
type Message interface{}

// Type PongMessage represents a
// pong message. It is returned by
// Read.
type PongMessage struct{}

// Type ConnectMessage represents a
// connection message. It is returned by
// Read.
type ConnectMessage struct{}

// Type DisconnectMessage represents a
// disconnection message. It is returned by
// Read.
type DisconnectMessage struct{}

// Type EventMessage represents an
// event message. It is returned by
// Read.
type EventMessage struct {
	Event string
	Data  []interface{}
}

// Type ErrorMessage represents an
// error  message. It is returned by
// Read.
type ErrorMessage struct{}
